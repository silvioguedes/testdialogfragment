package com.silvioapps.testdialogfragment;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainFragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragment = (MainFragment)fragmentManager.findFragmentById(R.id.fragment);
        if (fragment == null) {
            fragment = new MainFragment();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.fragment, fragment);
            fragmentTransaction.commit();
        } else {
            fragment = (MainFragment) fragmentManager.findFragmentById(R.id.fragment);
        }
    }
}
