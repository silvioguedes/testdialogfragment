package com.silvioapps.testdialogfragment;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.io.IOException;

/**
 * Created by Silvio Guedes on 10/07/2016.
 */
public class MainFragment extends Fragment {
    private View view = null;
    private CustomDialogFragment customDialogFragment = null;
    private NormalDialogFragment normalDialogFragment = null;
    private Button normalDialogFragmentButton = null;
    private Button customDialogFragmentButton = null;
    private boolean out = false;
    private CustomDialogFragmentTask customDialogFragmentTask = null;
    private NormalDialogFragmentTask normalDialogFragmentTask = null;
    private static MainFragment instance = null;
    private int progress = 0;
    private boolean isCustomDialog = true;

    public static MainFragment getInstance(){
        if(instance == null){
            instance = new MainFragment();
        }

        return instance;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        Log.i("TAG","onAttach");
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        setRetainInstance(true);

        customDialogFragment = CustomDialogFragment.getInstance();
        customDialogFragment.create(getFragmentManager(),CustomDialogFragment.FRAGMENT_TAG);
        customDialogFragment.setLayout(R.layout.custom_dialog_fragment_layout);
        customDialogFragment.setShowTitle(false);
        customDialogFragment.setShowText(true);
        customDialogFragment.setText("TEXT");
        customDialogFragment.setShowProgressBar(true);
        customDialogFragment.setProgress(0);

        normalDialogFragment = NormalDialogFragment.getInstance();
        normalDialogFragment.setLayout(R.layout.custom_dialog_fragment_layout);
        normalDialogFragment.setShowTitle(false);
        normalDialogFragment.setShowText(true);
        normalDialogFragment.setText("TEXT");
        normalDialogFragment.setShowProgressBar(true);
        normalDialogFragment.setProgress(0);

        Log.i("TAG","onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        Log.i("TAG","onCreateView");

        this.view = layoutInflater.inflate(R.layout.fragment_main, viewGroup, false);

        if(this.view != null) {
            normalDialogFragmentButton = (Button) this.view.findViewById(R.id.normalDialogFragmentButton);
            normalDialogFragmentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isCustomDialog = false;

                    normalDialogFragmentTask = new NormalDialogFragmentTask();
                    normalDialogFragmentTask.execute();
                }
            });

            customDialogFragmentButton = (Button) this.view.findViewById(R.id.customDialogFragmentButton);
            customDialogFragmentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isCustomDialog = true;

                    customDialogFragmentTask = new CustomDialogFragmentTask();
                    customDialogFragmentTask.execute();
                }
            });
        }

        return this.view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        Log.i("TAG", "onActivityCreated");
    }

    @Override
    public void onStart(){
        super.onStart();

        Log.i("TAG","onStart");
    }

    @Override
    public void onResume(){
        super.onResume();

        Log.i("TAG","onResume");
    }

    @Override
    public void onPause(){
        super.onPause();

        Log.i("TAG","onPause");
    }

    @Override
    public void onStop(){
        super.onStop();

        Log.i("TAG","onStop");
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();

        Log.i("TAG","onDestroyView");
    }

    @Override
    public void onDestroy(){
        out = true;

        if(customDialogFragmentTask != null){
            customDialogFragmentTask.cancel(true);
        }

        if(normalDialogFragmentTask != null){
            normalDialogFragmentTask.cancel(true);
        }

        super.onDestroy();

        Log.i("TAG","onDestroy");
    }

    @Override
    public void onDetach(){
        super.onDetach();

        Log.i("TAG","onDetach");
    }

    public boolean isCustomDialog(){
        return isCustomDialog;
    }

    public void customDialogFragmentPreExecuteTask(){
        if(customDialogFragment != null){
            customDialogFragment.setProgress(0);
            customDialogFragment.show();
        }
    }

    public void normalDialogFragmentPreExecuteTask(){
        if(normalDialogFragment != null){
            normalDialogFragment.setProgress(0);
            normalDialogFragment.show(getFragmentManager(), "");
        }
    }

    public void customDialogFragmentPostExecuteTask(){
        if(customDialogFragment != null){
            customDialogFragment.dismissDialog();
        }
    }

    public void normalDialogFragmentPostExecuteTask(){
        if(normalDialogFragment != null){
            normalDialogFragment.dismiss();
        }
    }

    public void customDialogFragmentBackgroundTask(Handler handler){
        progress = 0;

        try {
            getActivity().getAssets().list("");
        }
        catch(IOException e){
            e.printStackTrace();
        }

        while(!out && progress < 100){
            if(handler != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(customDialogFragment != null){
                            customDialogFragment.setProgress(progress);
                        }
                    }
                });
            }

            try{
                Thread.sleep(100);
            }
            catch(InterruptedException e){
                e.printStackTrace();
            }

            progress++;
        }
    }

    public void normalDialogFragmentBackgroundTask(Handler handler){
        progress = 0;

        try {
            getActivity().getAssets().list("");
        }
        catch(IOException e){
            e.printStackTrace();
        }

        while(!out && progress < 100){
            if(handler != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(normalDialogFragment != null){
                            normalDialogFragment.setProgress(progress);
                        }
                    }
                });
            }

            try{
                Thread.sleep(100);
            }
            catch(InterruptedException e){
                e.printStackTrace();
            }

            progress++;
        }
    }

    public class CustomDialogFragmentTask extends AsyncTask<String, Void, Object> {
        private Handler handler = new Handler();

        protected void onPreExecute() {
            customDialogFragmentPreExecuteTask();
        }

        protected Object doInBackground(String... args) {
            customDialogFragmentBackgroundTask(handler);

            return null;
        }

        protected void onPostExecute(Object args) {
            customDialogFragmentPostExecuteTask();
        }
    }

    public class NormalDialogFragmentTask extends AsyncTask<String, Void, Object> {
        private Handler handler = new Handler();

        protected void onPreExecute() {
            normalDialogFragmentPreExecuteTask();
        }

        protected Object doInBackground(String... args) {
            normalDialogFragmentBackgroundTask(handler);

            return null;
        }

        protected void onPostExecute(Object args) {
             normalDialogFragmentPostExecuteTask();
        }
    }
}
